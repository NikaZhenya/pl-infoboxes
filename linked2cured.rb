require 'json'

$hash = (JSON.parse File.read 'infoboxes_linked.json').compact
$raw  = (JSON.parse File.read 'infoboxes.json').compact
$out  = 'infoboxes_cured.json'

def add_year
  keys = [:first_appeared, :initial_release]
  key  = :year
  keys_to_sym $hash
  keys_to_sym $raw
  $raw.each do |e|
    keys.each do |k|
      y = get_year(e[k])
      i = get_e e[:lang] 
      $hash[i][key] = y if y && i
    end
  end
end

def keys_to_sym obj
  obj.map!{|e| e ? e.transform_keys!(&:to_sym) : nil}
end

def get_year e
  return e ? e[0].match(/\d{4}/).to_a.first.to_i : nil
end

def get_e lang
  return lang ? $hash.index($hash.reject{|e| e[:lang] != lang}.first) : nil
end

def get_lost_in_time
  lost = $hash.reject{|e| e && e[:year] != nil}
              .map{|e| e[:lang]}.sort
  puts "=> LOST IN TIME: #{lost.length}/#{$hash.length}", pretty(lost)
end

def pretty e
  return JSON.pretty_generate e
end

def save
  File.open($out, 'w'){|f| f.write pretty $hash}
end

add_year
get_lost_in_time
save
